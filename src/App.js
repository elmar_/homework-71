import React from "react";
import {Route, Switch} from "react-router-dom";
import Dishes from "./containers/Dishes/Dishes";
import Orders from "./containers/Orders/Orders";
import Layout from "./components/Layout/Layout";
import AddDish from "./containers/AddDish/AddDish";
import Edit from "./containers/Edit/Edit";


const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Dishes} />
            <Route path="/Orders" component={Orders} />
            <Route path="/AddDish" component={AddDish} />
            <Route path="/Edit/:id" component={Edit} />
        </Switch>
    </Layout>
);

export default App;

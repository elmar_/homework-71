import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {delDish, fetchDishes, orderedFalse} from "../../store/actions/dishesActions";
import Dish from "../../components/Dish/Dish";
import Spinner from "../../components/Spinner/Spinner";
import './Dishes.css';

const Dishes = props => {
    const dispatch = useDispatch();
    const dishes = useSelector(state => state.dishes.dishes);
    const loading = useSelector(state => state.dishes.loadingDish);

    useEffect(() => {
        dispatch(fetchDishes());
    }, [dispatch]);

    const buttonClicked = () => {
        dispatch(orderedFalse());
        props.history.push('/AddDish');
    };

    const Edit = id => {
        dispatch(orderedFalse());
        props.history.push('/Edit/' + id);
    };

    const deleteDish = (id, index) => {
        dispatch(delDish(id, index));
    };

    let menu = dishes.map((dish, index) => (
        <Dish dish={dish} key={dish.id} remove={deleteDish} index={index} edit={Edit} />
    ));

    if (loading) {
        menu = <Spinner />;
    }

    return (
        <div className="Dishes">
            <div className="between-block">
                <h3 className="title">Dishes</h3>
                <button className="btn-dishes" onClick={buttonClicked}>Add a new Dish</button>
            </div>
            {menu}
        </div>
    );
};

export default Dishes;
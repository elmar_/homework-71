import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchOrders, removeOrder} from "../../store/actions/ordersActions";
import {fetchDishes} from "../../store/actions/dishesActions";
import Order from "../../components/Order/Order";
import Spinner from "../../components/Spinner/Spinner";
import './Orders.css';

const Orders = () => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.orders.loading);
    const orders = useSelector(state => state.orders.orders);
    const dataOrders = useSelector(state => state.dishes.dishes);

    useEffect(() => {
        if (dataOrders.length === 0) {
            dispatch(fetchDishes());
        }
        // Первый dispatch нужен для получения меню (информация о цене и т.д.), так как в тз было указано в базе данных хранится ключь и кол-во, а при обновлении страницы пропадает state меню и не откуда будет брать цену и т.д.
        dispatch(fetchOrders());
    }, [dispatch, dataOrders]);

    const ordersToOrder = orders.map(order => {
        const ordersTrueTitle = [];
        let totalPrice = 150;
        for (const index in order.orders) {
            const ob = {};
            const titleId = dataOrders.find(i => i.id === index);
            if (titleId) {
                ob.title = titleId.title;
                ob.price = titleId.price;
                ob.id = index;
                if (order.orders.hasOwnProperty(index)) {
                    ob.amount = order.orders[index];
                    totalPrice = totalPrice + titleId.price * order.orders[index];
                }
            }
            ordersTrueTitle.push({...ob});
        }

        return {
            ordersTrueTitle,
            id: order.id,
            totalPrice: totalPrice
        };
    });

    const deleteOrder = (id, index) => {
        dispatch(removeOrder(id, index));
    };


    let ordersJSX = (
        ordersToOrder.map((order, index) => (
            <Order order={order} key={order.id} remove={deleteOrder} index={index} />
        )));

    if (loading) {
        ordersJSX = <Spinner />;
    }

    return (
        <div className="Orders">
            <div className="between-block">
                <h3 className="title">Orders</h3>
            </div>
            {ordersJSX}
        </div>
    );
};

export default Orders;
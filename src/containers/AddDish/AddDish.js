import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {saveDish} from "../../store/actions/dishesActions";
import Spinner from "../../components/Spinner/Spinner";
import Form from "../../components/Form/Form";
import './AddDish.css';

const AddDish = props => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.dishes.loadingSave);
    const saved = useSelector(state => state.dishes.saved);

    const save = (e, dish) => {
        e.preventDefault();

        dispatch(saveDish({...dish}));
    };

    let form = <Form submit={save} />;

    if (loading) {
        form = <Spinner />;
    }

    if (saved) {
        props.history.goBack();
    }

    return (
        <div className="AddDish">
            <h2>Add Dish page</h2>
            {form}
        </div>
    );
};

export default AddDish;
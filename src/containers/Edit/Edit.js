import React, {useEffect, useState} from 'react';
import Form from "../../components/Form/Form";
import {useDispatch, useSelector} from "react-redux";
import Spinner from "../../components/Spinner/Spinner";
import './Edit.css';
import {editDish, editDishSuccess, saveDishError, saveDishRequest} from "../../store/actions/dishesActions";
import axiosLink from "../../axiosLink";

const Edit = props => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.dishes.loadingSave);
    const changed = useSelector(state => state.dishes.saved);

    const [dish, setDish] = useState({});

    useEffect(() => {
        const getDish = async id => {
            try {
                dispatch(saveDishRequest());
                const response = await axiosLink.get('dishes/' + id + '.json');
                setDish(response.data);
                dispatch(editDishSuccess());
            } catch(e) {
                dispatch(saveDishError());
            }
        };

        getDish(props.match.params.id).catch();
    }, [props.match.params.id, dispatch]);


    const editedDish = (e, dish, id) => {
        e.preventDefault();

        dispatch(editDish(id, dish));
    };

    let form = <Form editDish={dish} submit={editedDish} id={props.match.params.id} />;


    if (loading) {
        form = <Spinner />
    }

    if (changed) {
        props.history.goBack();
    }

    return (
        <div className="Edit">
            <h2>Edit Dish page</h2>
            {form}
        </div>
    );
};

export default Edit;
import axiosLink from "../../axiosLink";

export const SAVE_DISH_REQUEST = 'SAVE_DISH_REQUEST';
export const SAVE_DISH_SUCCESS = 'SAVE_DISH_SUCCESS';
export const SAVE_DISH_ERROR = 'SAVE_DISH_ERROR';

export const FETCH_DISHES_REQUEST = 'FETCH_DISHES_REQUEST';
export const FETCH_DISHES_SUCCESS = 'FETCH_DISHES_SUCCESS';
export const FETCH_DISHES_ERROR = 'FETCH_DISHES_ERROR';

export const DELETE_DISH = 'DELETE_DISH';
export const DELETE_CATCH = 'DELETE_CATCH';

export const ORDERED_FALSE = 'ORDERED_FALSE';

export const EDIT_DISH_SUCCESS = 'EDIT_DISH_SUCCESS';



export const saveDishRequest = () => ({type: SAVE_DISH_REQUEST});
export const saveDishSuccess = () => ({type: SAVE_DISH_SUCCESS});
export const saveDishError = error => ({type: SAVE_DISH_ERROR, error});

export const fetchDishesRequest = () => ({type: FETCH_DISHES_REQUEST});
export const fetchDishesSuccess = data => ({type: FETCH_DISHES_SUCCESS, data});
export const fetchDishesError = error => ({type: FETCH_DISHES_ERROR, error});

export const deleteDish = index => ({type: DELETE_DISH, index});
export const deleteCatch = error => ({type: DELETE_CATCH, error});

export const orderedFalse = () => ({type: ORDERED_FALSE});

export const editDishSuccess = () => ({type: EDIT_DISH_SUCCESS});

export const saveDish = dish => {
    return async dispatch => {
        try {
            dispatch(saveDishRequest());
            await axiosLink.post('dishes.json', dish);
            dispatch(saveDishSuccess());
        } catch(e) {
            dispatch(saveDishError(e));
        }
    };
};

export const fetchDishes = () => {
    return async dispatch => {
        try {
            dispatch(fetchDishesRequest());
            const response = await axiosLink.get('dishes.json');
            const dishes = Object.keys(response.data).map(id => ({
                ...response.data[id],
                id
            }));
            dispatch(fetchDishesSuccess(dishes));
        } catch(e) {
            dispatch(fetchDishesError(e));
        }
    };
};

export const delDish = (id, index) => {
    return async dispatch => {
        try {
            await axiosLink.delete('dishes/' + id + '.json');
            dispatch(deleteDish(index));
        } catch (e) {
            deleteCatch(e);
        }
    };
};

export const editDish = (id, dish) => {
    return async dispatch => {
        try {
            dispatch(saveDishRequest());
            await axiosLink.put('dishes/' + id + '.json', dish);
            dispatch(saveDishSuccess());
        } catch (e) {
            dispatch(saveDishError(e));
        }
    }
}

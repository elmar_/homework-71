import axiosLink from "../../axiosLink";


export const FETCH_ORDER_REQUEST = 'FETCH_ORDER_REQUEST';
export const FETCH_ORDER_SUCCESS = 'FETCH_ORDER_SUCCESS';
export const FETCH_ORDER_ERROR = 'FETCH_ORDER_ERROR';

export const DELETE_ORDER = 'DELETE_ORDER';

export const fetchOrderRequest = () => ({type: FETCH_ORDER_REQUEST});
export const fetchOrderSuccess = orders => ({type: FETCH_ORDER_SUCCESS, orders});
export const fetchOrderError = error => ({type: FETCH_ORDER_ERROR, error});

export const deleteOrder = index => ({type: DELETE_ORDER, index});


export const fetchOrders = () => {
    return async dispatch => {
        try {
            dispatch(fetchOrderRequest());
            const response = await axiosLink.get('orders.json');
            const orders = Object.keys(response.data).map(id => ({
                orders: {...response.data[id]},
                id
            }));
            dispatch(fetchOrderSuccess(orders));
        } catch(e) {
            dispatch(fetchOrderError(e));
        }
    };
};

export const removeOrder = (id, index) => {
    return async dispatch => {
        try {
            await axiosLink.delete('orders/' + id + '.json');
            dispatch(deleteOrder(index));
        } catch (e) {
            throw (e);
        }
    };
};
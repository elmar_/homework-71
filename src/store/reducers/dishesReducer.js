import {
    DELETE_CATCH,
    DELETE_DISH, EDIT_DISH_SUCCESS,
    FETCH_DISHES_ERROR,
    FETCH_DISHES_REQUEST, FETCH_DISHES_SUCCESS,
    ORDERED_FALSE,
    SAVE_DISH_ERROR,
    SAVE_DISH_REQUEST,
    SAVE_DISH_SUCCESS
} from "../actions/dishesActions";

const initialState = {
    dishes: [],
    loadingSave: false,
    errorSave: false,
    saved: false,
    loadingDish: false,
    errorDish: false
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DISHES_REQUEST:
            return {...state, loadingDish: true};
        case FETCH_DISHES_SUCCESS:
            return {...state, loadingDish: false, dishes: action.data};
        case FETCH_DISHES_ERROR:
            return {...state, loadingDish: false, errorDish: action.error};
        case SAVE_DISH_REQUEST:
            return {...state, loadingSave: true};
        case SAVE_DISH_SUCCESS:
            return {...state, loadingSave: false, saved: true};
        case SAVE_DISH_ERROR:
            return {...state, loadingSave: false, errorSave: action.error};
        case ORDERED_FALSE:
            return {...state, saved: false};
        case DELETE_DISH:
            const dishesCopy = [...state.dishes];
            dishesCopy.splice(action.index, 1);
            return {...state, dishes: dishesCopy};
        case EDIT_DISH_SUCCESS:
            return {...state, loadingSave: false};
        case DELETE_CATCH:
            return {...state, errorDish: true};
        default:
            return state;
    }
};

export default dishesReducer;
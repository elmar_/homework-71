import {DELETE_ORDER, FETCH_ORDER_ERROR, FETCH_ORDER_REQUEST, FETCH_ORDER_SUCCESS} from "../actions/ordersActions";

const initialState = {
    orders: [],
    loading: false,
    error: null
};

const ordersReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ORDER_REQUEST:
            return {...state, loading: true};
        case FETCH_ORDER_SUCCESS:
            return {...state, orders: action.orders, loading: false};
        case FETCH_ORDER_ERROR:
            return {...state, error: action.error, loading: false};
        case DELETE_ORDER:
            const newOrders = [...state.orders];
            newOrders.splice(action.index, 1);
            return {...state, orders: newOrders};
        default:
            return state;
    }
};

export default ordersReducer;
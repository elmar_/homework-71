import React, {useState} from 'react';
import './Form.css';

const Form = props => {

    const [formDish, setFormDish] = useState(props.editDish || {
        title: '',
        price: '',
        image: ''
    });


    const changeInput = e => {
        const {name, value} = e.target;

        setFormDish({
            ...formDish,
            [name]: value
        });
    };

    return (
        <form onSubmit={e => props.submit(e, formDish, props.id)}>
            <input
                className="Input" placeholder="Dish name"
                type="text" name="title"
                value={formDish.title}
                onChange={changeInput}
            />
            <input
                className="Input" placeholder="Dish price"
                type="number" name="price"
                value={formDish.price}
                onChange={changeInput}
            />
            <input
                className="Input" placeholder="Dish image"
                type="text" name="image"
                value={formDish.image}
                onChange={changeInput}
            />
            <button className="form-btn" type="submit">Save</button>
        </form>
    );
};

export default Form;
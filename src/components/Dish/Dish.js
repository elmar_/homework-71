import React from 'react';
import './Dish.css';

const Dish = ({dish, remove, index, edit}) => {
    return (
        <div className="Dish">
            <div className="box-1">
                <img src={dish.image} alt="#" className="img" />
                <h5 className="dish-name">{dish.title}</h5>
            </div>
            <div className="box-2">
                <span className="price">{dish.price}</span>
                <span className="CD" onClick={() => edit(dish.id)}>Edit</span>
                <span className="delete" onClick={() => remove(dish.id, index)}>Delete</span>
            </div>
        </div>
    );
};

export default Dish;
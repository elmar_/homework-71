import React from 'react';
import './Order.css';

const Order = ({order, remove, index}) => {

    const orders = order.ordersTrueTitle;

    //не знаю почему тут ошибка про key

    return (
        <div className="Order">
            <ul className="list-order">
                {orders.map(value => (
                    <li key={value.id}>{value.title}</li>
                ))}
                <li>Доставка:</li>
            </ul>
            <ul className="list-order">
                {orders.map(value => (
                    <li key={value.id}>{value.amount}</li>
                ))}
            </ul>
            <ul className="list-order">
                {orders.map(value => (
                    <li key={value.id}>{value.price} сом</li>
                ))}
                <li>150 сом</li>
            </ul>
            <ul className="list-order">
                <li>Order total:</li>
                <li><b>{order.totalPrice}</b></li>
                <li className="delete-order" onClick={() => remove(order.id, index)}>Complete order</li>
            </ul>
        </div>
    );
};

export default Order;
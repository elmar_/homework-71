import React from 'react';
import './Layout.css';
import {Link} from "react-router-dom";

const Layout = props => {
    return (
        <div className="container">
            <header className="header">
                <Link to="/" className="logo">Turtle Pizza Admin</Link>
                <ul className="list">
                    <li><Link to="/">Dishes</Link></li>
                    <li><Link to="/orders">Orders</Link></li>
                </ul>
            </header>
            {props.children}
        </div>
    );
};

export default Layout;
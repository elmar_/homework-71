import axios from "axios";

const axiosLink = axios.create({
    baseURL: 'https://classwork-63-burger-default-rtdb.firebaseio.com/homework-71/'
});

export default axiosLink;